//
//  AlbumViewController.swift
//  DemoGallery
//
//  Created by Marko Zec on 10/6/16.
//  Copyright 2016 Marko Zec. All rights reserved.
//

import UIKit
import Photos

class AlbumViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, GalleryManagerDataSource
{
    let cellIdentifier = "AlbumCell"
    
    let galleryManager = GalleryManager()
    
    var albumName: String?
    var albumType: PHAssetCollectionType?
    
    @IBOutlet weak var albumCollectionView: UICollectionView!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    deinit {
        GalleryData.removeAll()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        galleryManager.dataSource = self
        
        self.title = String(self.albumName!)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        if galleryManager.didAlbumExist(albumType: albumType!, albumName: albumName!) {
            loadAlbumDataAsync()
        } else {
            showAlertAndReturnToGallery()
        }
        
        infoLabel.hidden = true
        spinner?.startAnimating()
    }
    
    func galleryManager(currentImage image: UIImage, imageAsset asset: PHAsset, imageSize size: CGSize) {
        GalleryData.insert(creationDate: asset.creationDate, imageData: image)
    }

    private func loadAlbumDataAsync() {
        GalleryData.removeAll()
        dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INTERACTIVE, 0)) {
            let albumIsFetched = self.galleryManager.fetchAlbumPhotos(albumType: self.albumType!, albumTitle: self.albumName!)
            dispatch_async(dispatch_get_main_queue()) {
                if albumIsFetched {
                    self.spinner?.stopAnimating()
                    self.updateUI()
                } else {
                    self.showAlertAndReturnToGallery()
                }
            }
        }
    }
    
    private func updateUI() {
        self.albumCollectionView.reloadData()
        self.albumCollectionView.setNeedsLayout()
        self.albumCollectionView.setNeedsDisplay()
        self.performSelector(#selector(setupCells), withObject: nil, afterDelay: 0.01)
        
        self.infoLabel.hidden = false
        if GalleryData.count() > 0 {
            self.spinner?.stopAnimating()
            self.infoLabel.hidden = true
        }
    }
    
    func setupCells() {
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsetsMake(5, 5, 5, 5)
        let width = self.albumCollectionView.frame.size.width
        layout.itemSize = CGSize(width: width / 3 - 10 , height: width / 3 - 10)
        layout.minimumInteritemSpacing = 10
        layout.minimumLineSpacing = 5
        self.albumCollectionView.collectionViewLayout = layout
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return GalleryData.count()
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(cellIdentifier, forIndexPath: indexPath) as! AlbumCollectionViewCell
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, willDisplayCell cell: UICollectionViewCell, forItemAtIndexPath indexPath: NSIndexPath) {
        if let myCell = cell as? AlbumCollectionViewCell {
            if let imageData = GalleryData.get(atIndex: indexPath.row) {
                myCell.imageData = imageData
                myCell.updateCell()
            }
        }
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let imageViewController = self.storyboard!.instantiateViewControllerWithIdentifier("ImageViewController") as! ImageViewController
        self.navigationController!.pushViewController(imageViewController, animated: true)
        
        imageViewController.defultIndexPath = indexPath
        
        if let dateForTitle = GalleryData.get(atIndex: indexPath.row)?.creationDate {
            imageViewController.title = GalleryData.getDateAsLocalizedString(dateForTitle)
        } else {
            imageViewController.title = ""
        }
    }
    
    private func showAlertAndReturnToGallery() {
        let errorTitle = "Error occurred"
        let errorMessage = "Gallery is modified by some other application, this album is not available anymore"
        
        let alert = UIAlertController(title: errorTitle, message: errorMessage, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: .Default) { (action) in
            self.navigationController!.popViewControllerAnimated(true)
            })
        
        self.presentViewController(alert, animated: true, completion: nil)
    }
}
