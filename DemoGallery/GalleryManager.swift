//
//  GalleryManager.swift
//  DemoGallery
//
//  Created by Marko Zec on 10/14/16.
//  Copyright © 2016 Marko Zec. All rights reserved.
//

import Foundation
import Photos

protocol GalleryManagerDataSource: class {
    func galleryManager(currentImage image: UIImage, imageAsset asset: PHAsset, imageSize size: CGSize)
}

class GalleryManager {
    
    private var assetCollection = PHAssetCollection()
    private var photoAssets = PHFetchResult()
    
    weak var dataSource: GalleryManagerDataSource?
    
    func fetchAlbums(albumType type: PHAssetCollectionType) -> [String] {
        var array = [String]()
        let album = PHAssetCollection.fetchAssetCollectionsWithType(type, subtype: .Any, options: nil)
        
        if album.count > 0 {
            for n in 0..<album.count {
                let collection = album.objectAtIndex(n) as! PHCollection
                array.append(collection.localizedTitle!)
            }
        }
        
        return array
    }
    
    func didAlbumExist(albumType type: PHAssetCollectionType, albumName name: String) -> Bool {
        var albumExists = false
        let album = PHAssetCollection.fetchAssetCollectionsWithType(type, subtype: .Any, options: nil)
        
        if album.count > 0 {
            for n in 0..<album.count {
                let collection = album.objectAtIndex(n) as! PHCollection
                if name == collection.localizedTitle! {
                    albumExists = true
                }
            }
        }
        
        return albumExists
    }
    
    func fetchAlbumPhotos(albumType type: PHAssetCollectionType, albumTitle album: String) -> Bool {
        if let dataSourceObject = dataSource {
            /* Za Smart albume, ako znas jedinstveni nacin pomozi... */
            if type == .SmartAlbum {
                let smartAlbums = PHAssetCollection.fetchAssetCollectionsWithType(.SmartAlbum, subtype: .Any, options: nil)
                var smartAlbumFound = false
                
                if smartAlbums.count > 0 {
                    for n in 0..<smartAlbums.count {
                        let collection = smartAlbums.objectAtIndex(n) as! PHCollection
                        if collection.localizedTitle == album {
                            assetCollection = collection as! PHAssetCollection
                            smartAlbumFound = true
                        }
                    }
                }
                
                if !smartAlbumFound {
                    NSLog("Error: fetchAlbumPhotos() SmartAlbum \"\(album)\" not found")
                    return false
                }
            /* Za ostale albume */
            } else {
                let fetchOptions = PHFetchOptions()
                fetchOptions.predicate = NSPredicate(format: "title = %@", album)
                let collection = PHAssetCollection.fetchAssetCollectionsWithType(.Album, subtype: .Any, options: fetchOptions)
                
                if let firstObject = collection.firstObject {
                    assetCollection = firstObject as! PHAssetCollection
                } else { // ovde nikad ne bi trebao da upadne jer kontroler moze da koristi metodu za proveru albuma
                    NSLog("Error: fetchAlbumPhotos() Album \"\(album)\" not found")
                    return false
                }
            }
            
            photoAssets = PHAsset.fetchAssetsInAssetCollection(assetCollection, options: nil)
            let imageManager = PHCachingImageManager()
            
            photoAssets.enumerateObjectsUsingBlock {(object: AnyObject!, count: Int, stop: UnsafeMutablePointer<ObjCBool>) in
                
                if object is PHAsset {
                    let asset = object as! PHAsset
                    let imageSize = CGSize(width: asset.pixelWidth, height: asset.pixelHeight)
                    
                    /* For faster performance, and maybe degraded image */
                    let options = PHImageRequestOptions()
                    options.deliveryMode = .FastFormat
                    options.synchronous = true
                    
                    imageManager.requestImageForAsset(asset, targetSize: imageSize, contentMode: .AspectFit, options: options) {
                            (image, info) -> Void in
                            dataSourceObject.galleryManager(currentImage: image!, imageAsset: asset, imageSize: imageSize)
                    }
                }
            }
        
        } else {
            NSLog("Error: fetchAlbumPhotos() needs a dataSource object, you have to implement GalleryManagerDataSource")
            return false
        }
        
        return true
    }
    
}
