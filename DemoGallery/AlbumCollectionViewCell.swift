//
//  ImageCollectionViewCell.swift
//  DemoGallery
//
//  Created by Marko Zec on 10/6/16.
//  Copyright 2016 Marko Zec. All rights reserved.
//

import UIKit

class AlbumCollectionViewCell: UICollectionViewCell
{
    @IBOutlet weak var imageOfTheCell: UIImageView!
    
    var imageData: GalleryImageData?
    
    func updateCell() {
        if imageData != nil {
            self.imageOfTheCell.image = imageData!.image
        }
    }
        
}
