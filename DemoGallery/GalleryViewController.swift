//
//  GalleryViewController.swift
//  DemoGallery
//
//  Created by Marko Zec on 10/11/16.
//  Copyright © 2016 Marko Zec. All rights reserved.
//

import UIKit
import Photos

class GalleryViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {

    let cellIdentifier = "GalleryCell"
    var albums = [[String]]()
    
    let galleryManager = GalleryManager()
    
    @IBOutlet weak var galleryCollectionView: UICollectionView!
    
    @IBAction func btnRefresh(sender: UIBarButtonItem) {
        self.loadAlbums()
        self.galleryCollectionView.reloadData()
//        self.galleryCollectionView.setNeedsDisplay()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.loadAlbums() // i inicijalno i ako je doslo do promene po povratku u galeriju
        self.performSelector(#selector(setupCells), withObject: nil, afterDelay: 0.01)
    }
    
    private func loadAlbums() {
        albums.removeAll()
        
        var smartAlbums = [String]()
        var userAlbums = [String]()
        
        smartAlbums = galleryManager.fetchAlbums(albumType: PHAssetCollectionType.SmartAlbum)
        userAlbums = galleryManager.fetchAlbums(albumType: PHAssetCollectionType.Album)
        
        albums.append(userAlbums)
        albums.append(smartAlbums)
    }
    
    @objc private func setupCells() {
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsetsMake(5, 5, 5, 5)
        let width = self.galleryCollectionView.frame.size.width
        layout.itemSize = CGSize(width: width / 3 - 10 , height: width / 3 - 10)
        layout.minimumInteritemSpacing = 10
        layout.minimumLineSpacing = 5
        self.galleryCollectionView.collectionViewLayout = layout
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return albums.count
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return albums[section].count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(cellIdentifier, forIndexPath: indexPath) as! GalleryCollectionViewCell
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, willDisplayCell cell: UICollectionViewCell, forItemAtIndexPath indexPath: NSIndexPath) {
        if let myCell = cell as? GalleryCollectionViewCell {
            myCell.albumName = albums[indexPath.section][indexPath.row]
            myCell.updateCell()
        }
    }
    
    func collectionView(collectionView: UICollectionView, shouldSelectItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        let albumName = albums[indexPath.section][indexPath.row]
        var albumType = PHAssetCollectionType.Album
        if indexPath.section == 1 {
            albumType = PHAssetCollectionType.SmartAlbum
        }
        
        if !galleryManager.didAlbumExist(albumType: albumType, albumName: albumName) {
            showAlertForNotExistingAlbum()
            return false
        }
        return true
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let albumViewController = self.storyboard!.instantiateViewControllerWithIdentifier("AlbumViewController") as! AlbumViewController
        
        let section = indexPath.section
        let row = indexPath.row
        
        albumViewController.albumName = albums[section][row]
        albumViewController.albumType = PHAssetCollectionType.Album
        if indexPath.section == 1 {
            albumViewController.albumType = PHAssetCollectionType.SmartAlbum
        }
        
        self.navigationController!.pushViewController(albumViewController, animated: true)
    }
    
    private func showAlertForNotExistingAlbum() {
        let errorTitle = "Error occurred"
        let errorMessage = "Gallery is modified by some other application, selected album is not available anymore"
        
        let alert = UIAlertController(title: errorTitle, message: errorMessage, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: .Default) { (action) in
            self.loadAlbums()
            self.galleryCollectionView.reloadData()
        })
        
        self.presentViewController(alert, animated: true, completion: nil)
    }
}
