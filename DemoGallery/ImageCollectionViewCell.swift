//
//  ImageCollectionViewCell.swift
//  DemoGallery
//
//  Created by Marko Zec on 10/11/16.
//  Copyright © 2016 Marko Zec. All rights reserved.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageOfTheCell: UIImageView!
    
    var imageData: GalleryImageData?
    
    func updateCell() {
        if let image = imageData?.image {
            self.imageOfTheCell.image = image
        }
    }

}
