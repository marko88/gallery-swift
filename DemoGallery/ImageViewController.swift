//
//  ImageViewController.swift
//  DemoGallery
//
//  Created by Marko Zec on 10/6/16.
//  Copyright 2016 Marko Zec. All rights reserved.
//

import UIKit

class ImageViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UIScrollViewDelegate
{
    
    @IBOutlet weak var imagesCollectionView: UICollectionView!
    
    var defultIndexPath: NSIndexPath?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.imagesCollectionView.pagingEnabled = true
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.performSelector(#selector(setupCells), withObject: nil, afterDelay: 0.01)
        imagesCollectionView.scrollToItemAtIndexPath(defultIndexPath!, atScrollPosition: .CenteredHorizontally, animated: false)
    }
    
    func setupCells() {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .Horizontal
        layout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0)
        layout.itemSize = self.imagesCollectionView.frame.size
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        self.imagesCollectionView.collectionViewLayout = layout
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return GalleryData.count()
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("GalleryCell", forIndexPath: indexPath) as! ImageCollectionViewCell
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, willDisplayCell cell: UICollectionViewCell, forItemAtIndexPath indexPath: NSIndexPath) {
        let myCell = cell as! ImageCollectionViewCell
        if let imageData = GalleryData.get(atIndex: indexPath.row) {
            myCell.imageData = imageData
            myCell.updateCell()
        }
        
        let backToGalleryGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(ImageViewController.backToGallary))
        backToGalleryGestureRecognizer.direction = .Down
        myCell.userInteractionEnabled = true
        myCell.addGestureRecognizer(backToGalleryGestureRecognizer)
        
        // mozda je ovo moglo bolje
        if let stratingImageIndexPath = defultIndexPath {
            imagesCollectionView.scrollToItemAtIndexPath(stratingImageIndexPath, atScrollPosition: .CenteredHorizontally, animated: false)
            defultIndexPath = nil
        }
    }
    
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        let cell = self.imagesCollectionView.visibleCells().first as? ImageCollectionViewCell
        let indexPath = self.imagesCollectionView.indexPathForCell(cell!)
        
        if GalleryData.count() > 0 { // bez ovoga mi puca gestikulacija na dole, jer nekako izbirsem slike pre ovoga i onda index out of range...
            if let dateForTitle = GalleryData.get(atIndex: indexPath!.row)?.creationDate {
                self.title = GalleryData.getDateAsLocalizedString(dateForTitle)
            } else {
                self.title = ""
            }
        }
    }
    
    func backToGallary() {
        self.navigationController!.popViewControllerAnimated(true)
    }

}
