//
//  GalleryData.swift
//  DemoGallery
//
//  Created by Marko Zec on 10/7/16.
//  Copyright 2016 Marko Zec. All rights reserved.
//

import UIKit

class GalleryData
{
    private static let instance = GalleryData()
    
    private var array = [GalleryImageData]()
    private var pointer = 0
    
    private init() {}
    
    static func insert(creationDate date: NSDate?, imageData img: UIImage?) {
        instance.insert(creationDate: date, imageData: img)
    }
    
    static func removeAll() {
        instance.array.removeAll()
    }
    
    static func get(atIndex index: Int) -> GalleryImageData? {
        return instance.get(atIndex: index)
    }
    
    static func count() -> Int {
        return instance.array.count
    }
    
    static func getDateAsLocalizedString(date: NSDate) -> String { // ovoj util fji nije ovde mesto, ali posto je jedina...
        let dateFormatter = NSDateFormatter()
        dateFormatter.locale = NSLocale.currentLocale()
        dateFormatter.timeZone = NSTimeZone.defaultTimeZone()
        dateFormatter.dateStyle = .LongStyle
        let formattedDate = dateFormatter.stringFromDate(date)
        
        return formattedDate
    }
    
    private func insert(creationDate date: NSDate?, imageData img: UIImage?) {
        array.append(GalleryImageData(creationDate: date, image: img))
    }
    
    private func get(atIndex index: Int) -> GalleryImageData? {
        pointer = index
        return array[index]
    }
    
    // Metode ispod trenutno nemaju primenu
    
    private func next() -> GalleryImageData? {
        if array.last?.creationDate != array[pointer].creationDate {
            pointer += 1
            return array[pointer]
        }
        return nil
    }
    
    private func previous() -> GalleryImageData? {
        if array.first?.creationDate != array[pointer].creationDate {
            pointer -= 1
            return array[pointer]
        }
        return nil
    }
    
    private func hasNext() -> Bool {
        if array.last?.creationDate == array[pointer].creationDate {
            return false
        }
        return true
    }
    
    private func hasPrevious() -> Bool {
        if array.first?.creationDate == array[pointer].creationDate {
            return false
        }
        return true
    }
    
}

class GalleryImageData {
    var creationDate: NSDate?
    var image: UIImage? // profan sa kursa rece da je losa praksa da ubacujes UIKit u model, jer ui elementima nije mesto u modelu, sto i sam znas... bilo je import Foundation, zato sam stavio AnyObject...
    
    init(creationDate: NSDate?, image: UIImage?) {
        self.creationDate = creationDate
        self.image = image
    }
}
