//
//  GalleryCollectionViewCell.swift
//  DemoGallery
//
//  Created by Marko Zec on 10/11/16.
//  Copyright © 2016 Marko Zec. All rights reserved.
//

import UIKit

class GalleryCollectionViewCell: UICollectionViewCell {
    
    var albumName: String?
    
    @IBOutlet weak var albumNameLabel: UILabel!
    @IBOutlet weak var albumImage: UIImageView!
    
    func updateCell() {
        self.albumNameLabel.text = self.albumName
    }
}
